import os

directory = "public/"

with open(os.path.join(directory, "index.html"), "w") as index_file:
    index_file.write("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\">\n")
    index_file.write("<html>\n")
    index_file.write("<head>\n")
    index_file.write("<title>Directory listing</title>\n")
    index_file.write("</head>\n")
    index_file.write("<body>\n")
    index_file.write("<h1>Directory listing</h1>\n")
    index_file.write("<ul>\n")

    for file in os.listdir(directory):
        if file == 'index.html':
            continue
        if file.endswith(".html") or file.endswith(".pdf") or file.endswith(".md"):
            index_file.write(f"<li><a href='{file}'>{file}</a></li>\n")

    index_file.write("</ul>\n")
    index_file.write("</body>\n")
    index_file.write("</html>\n")

print("index.html created")
