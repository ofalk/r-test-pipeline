# R Test Pipeline

## Description
Sample project showing how, with very low effort, one can use the powers of GitLab CI/CD to render an deploy
a very simple RMarkdown script into HTML and deploy it with [GitLab pages](https://ofalk.gitlab.io/r-test-pipeline)

## Badges

Current pipline status on main branch: ![pipeline status](https://gitlab.com/ofalk/r-test-pipeline/badges/main/pipeline.svg)

## Support
This is completely unsupported, but I'm happy if you reuse the idea and/or contribute to it.

## License
This was mainly created using RStudio and the R language, hence: AGPL v3

## Project status
No further plans to extend this in any way, feel free to collaborate and/or make suggestions and should I dive into the topic
again, I may enhance it accordingly, but there are no *plans*.
